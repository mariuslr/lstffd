List File Field Description (LSTFFD) lists the file field descriptions on the screen, 
with an option to print. LSTFFD lists the file name, library, file type 
(physical, logical, etc), whether or not it is uniquely keyed, field name, 
field description, field type, field length, whether or not it is a key field, and the 
key sequence.

LSTFFD has an option to display database relations, triggers attached to the file, 
and will also diplay the data using Query/400 with or without selection.

See more at www.bvstools.com.

1.  Location of Source.                                   
                                                          
    PC File Name        Member    Type    Source File         
    ------------        --------- ----    -----------         
    QCMDSRC.LSTFFD      LSTFFD    CMD     QCMDSRC             
    QCLSRC.FFD001CL     FFD001CL  CLP     QCLSRC              
    QDSPSRC.FFD001DF    FFD001DF  DSPF    QDSPSRC             
    QDDSSRC.FFD001P1    FFD001P1  PRTF    QDDSSRC  
    QRPGLESRC.FFD001RG  FFD001RG  RPGLE   QRPGLESRC           
                                                          
2.  Compile FFD001CL.                                     
    CRTCLPGM PGM(XXX/FFD001CL) SRCFILE(XXX/QCLSRC)        
                                                          
3.  Create Command LSTFFD.                                
    CRTCMD CMD(XXX/LSTFFD) PGM(XXX/FFD001CL)             
       SRCFILE(XXX/QCMDSRC)                               
                                                          
4.  Compile FFD001DF.                                     
    CRTDSPF FILE(XXX/FFD001DF) SRCFILE(XXX/QDDSSRC)       
                                                          
5.  Compile FFD001P1.                                     
    CRTPRTF FILE(XXX/FFD001P1) SRCFILE(XXX/QDDSSRC)      
       PAGESIZE(65 80)                                    
                                                          
6.  Compile FFD001RG.                                     
    CRTBNDRPG PGM(XXX/FFD001RG) SRCFILE(XXX/QRPGLESRC)   
     DFTACTGRP(*NO) ACTGRP(*NEW)          
     
Copyright (c) 1999-2016, BVSTools, Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that 
the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the 
following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.